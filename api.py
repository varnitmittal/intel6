from flask import Flask, request, render_template, jsonify
from keras.models import load_model
from keras.preprocessing.image import img_to_array
from keras.applications import imagenet_utils
import tensorflow as tf
from PIL import Image
import numpy as np
import io
import os

app = Flask(__name__)
app_ROOT = os.path.dirname(os.path.abspath(__file__))

def get_model():
    global model, graph, session
    model = load_model('./model/intel6_model.h5')
    model._make_predict_function()
    graph = tf.get_default_graph()
    session = tf.Session()
    print("Neural Network Initialized")

def prepare_image(image, target):
    if image.mode != "RGB":
        image = image.convert("RGB")
    image = image.resize(target)
    image = img_to_array(image)
    image = np.expand_dims(image, axis=0)
    return image

@app.route("/", methods=['GET'])
def index():
    return render_template("upload.html")

@app.route("/upload", methods=["POST"])
def upload():
    target = os.path.join(app_ROOT, 'userimages/')
    if not os.path.isdir(target):
        os.mkdir(target)
    for file in request.files.getlist("file"):
        filename = file.filename
        ext = os.path.splitext(filename)[1]
        if not ((ext == ".jpg") or (ext == ".png") or (ext == ".jpeg")):
            return jsonify({"Status": "Success", "Message": "File format not supported."})
        destination = "/".join([target, filename])
        file.save(destination)
    return jsonify({"Status": "Success"})

@app.route("/predict", methods=["POST"])
def predict():
#    for file in request.files.getlist("file"):
    image = request.files["file"].read()
    image = Image.open(io.BytesIO(image))
    image = prepare_image(image, target=(256, 256))
    with graph.as_default():
#        with session.as_default():
        preds = model.predict(image)
    result = np.where(preds == np.amax(preds))
    final_result = int(result[1][0])
    return jsonify({"prediction": final_result})

get_model()
if __name__ == "__main__":
    port = int(os.environ.get('PORT', 33507))
    app.run(port=port)
